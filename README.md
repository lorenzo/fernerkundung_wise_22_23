# Spezielle Themen der Fernerkundung - WiSe 22/23

Autoren: Gerda Gerz und Lorenzo Melchior

Kursleiterin: Dr. Marion Stellmes

### Beschreibung

In diesem Verzeichnis sind die Skripte hinterlegt, welche benutzt worden sind um die Daten und Grafiken der Abschlussaufgabe zu erstellen. 

##### ForestLossByYear (JavaScript)

Berechnet die Abholzung von Brasilien über einen gestzten Zeitraum und exportiert diese als CSV in die Google Drive. 

##### DeforestationMap (JavaScript)

Erstellt eine Karte der Abholzung von Brasilien basierend auf dem Hansen et. al. Datensatz. 

##### DatenVisualisierung (Python - Jupyter Notebook)

Erstellt ein Histogramm aus den Daten von ForestLossByYear.js.
