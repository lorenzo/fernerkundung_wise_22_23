// Author: Gerda Gerz und Lorenzo Melchior
// Dieses Skript muss in der Google Earth Engine ausgeführt werden.
// Es wird der Waldverlust pro Jahr für ein gegebenes Land in einem gegebenen Zeitraum berechnet 
// und als CSV nach Google Drive exportiert. 

// Konfiguration
var countryName = 'Brazil';
var startYear = 2000;
var endYear = 2022;

// Grenzwert für die Klassifikation
var eviThreshold = 0.6;

// Geometrische Form für das konfigurierte Land laden
var countries = ee.FeatureCollection("USDOS/LSIB_SIMPLE/2017");
var geometry = countries.filterMetadata('country_na', 'equals', countryName).geometry();


// Funktion die den EVI anhand eines Landsat 7 Bildes berechnet
function calculateEVI(image) {
  var evi = image.expression(
    '2.5 * ((NIR - RED) / (NIR + 6 * RED - 7.5 * BLUE + 1))',
    {
      'NIR': image.select('B4'),
      'RED': image.select('B3'),
      'BLUE': image.select('B1')
    }
  ).rename('EVI');
  return image.addBands(evi);
}

// DEPRECATED: Switched to EVI
// Funktion die den NDVI anhand eines Landsat 7 Bildes berechnet
function calculateNDVI(image) {
  var ndvi = image.normalizedDifference(['B4', 'B3']).rename('NDVI');
  return image.addBands(ndvi);
}

// Funktion die eine Binärmaske erstellt für Pixel die den EVI Grenzwert erreichen.
// Ausgabe ist ein Bild welches Vegetation / nicht-Vegetation binär klassifiziert.
function createBinaryMap(image) {
  return image.select('EVI').gt(eviThreshold).rename('forest_map');
}

// Berechnung des jährlichen Waldverlusts.
// Erst werden die EVI Bilder von zwei aufeinander folgenden Jahren erstellt.
// Dabei wird für jeden Pixel der Median des EVI gewählt.
// Dann werden Binärmasken der beiden Bilder erstellt 
function computeForestLoss(year) {
  var startDate = ee.Date.fromYMD(year, 1, 1);
  var endDate = ee.Date.fromYMD(year + 1, 1, 1);

  // EVI für 2 aufeinander folgende Jahre berechnen. 
  // Dabei wird der Median jedes Pixel über den Zeitraum gewählt.
  var image1 = ee.ImageCollection('LANDSAT/LE07/C02/T1_TOA')
    .filterDate(startDate, endDate)
    .filterBounds(geometry)
    .map(calculateEVI)
    .select('EVI')
    .median()
    .rename('EVI');

  var image2 = ee.ImageCollection('LANDSAT/LE07/C02/T1_TOA')
    .filterDate(startDate.advance(1, 'year'), endDate.advance(1, 'year'))
    .filterBounds(geometry)
    .map(calculateEVI)
    .select('EVI')
    .median()
    .rename('EVI');

  // Klassifikation aller Pixel (vegetation / keine-vegetation)
  var binaryMap1 = createBinaryMap(image1).unmask(0);
  var binaryMap2 = createBinaryMap(image2).unmask(0);

  // Alle Pixel finden die im ersten Jahr als Vegetation klassifiziert worden sind
  // und im zweiten nicht mehr -> Waldverlust
  var forestLoss = binaryMap1.subtract(binaryMap2).gt(0);
  return forestLoss.set('system:time_start', startDate.millis());
}

// Waldverlust für alle Jahre im Zeitraum berechnen.
var annualForestLoss = [];
var xLabels = [];
for (var year = startYear; year <= endYear; year++) {
  annualForestLoss.push(computeForestLoss(year));
  xLabels.push(year.toString());
}

// Funktion die Waldverlust als ee.Image summiert und in Quadratkilometer umrechnet.
function lossToFeature(lossImage, index) {
  var loss = lossImage.reduceRegion({
    reducer: ee.Reducer.sum(),
    geometry: geometry,
    scale: 30,
    maxPixels: 1e12
  });
  var year = xLabels[index];
  var lossInSquareKilometers = ee.Number(loss.get('forest_map')).multiply(0.0009);
  return ee.Feature(null, {'year': year, 'forest_loss': lossInSquareKilometers});
}

// lossToFeature Funktion auf alle Daten anwenden.
var forestLossFeatures = ee.FeatureCollection(annualForestLoss.map(lossToFeature));

// Daten als CSV in Google Drive speichern. 
Export.table.toDrive({
  collection: forestLossFeatures,
  description: 'brazil_forest_loss',
  fileNamePrefix: 'brazil_forest_loss',
  fileFormat: 'CSV',
});
