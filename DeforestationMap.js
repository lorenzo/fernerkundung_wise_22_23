// Authors: Gerda Gerz und Lorenzo Melchior
// Dieses Skript muss in der Google Earth Engine ausgeführt werden.
// Die Übersichtskarte von GEE wird dabei mit einer Grafik ergenzt,
// welche den Waldverlust nach Hansen et. al. für die zwei Zeiträume 
// 2017-2018 und 2019-2020 darstellt.

// Zentrum von Brasilien
Map.setCenter(-51.9253, -14.2350, 5);

// Geometrische Form für das konfigurierte Land laden
var countries = ee.FeatureCollection('USDOS/LSIB_SIMPLE/2017');
var brazil = countries.filter(ee.Filter.eq('country_na', 'Brazil'));

// Hansen et. al Datensatz
var dataset = ee.Image('UMD/hansen/global_forest_change_2021_v1_9');
dataset = dataset.clip(brazil);

var bandName = 'lossyear';

// Binärmasken für Wladverlust erstellen
var maskBolsonaro = dataset.select(bandName).gte(19).and(dataset.select(bandName).lte(21));
var maskNotBolsonaro = dataset.select(bandName).gte(16).and(dataset.select(bandName).lte(18));

var band = dataset.select("lossyear");

// Binärmasken auf "lossyear" band anwenden
var bolsonaro = band.where(maskBolsonaro, 0);
var notBolsonaro = band.where(maskNotBolsonaro, 0);

// Bewaldung darstellen
var treeCoverVisParam = {
  bands: ['treecover2000'],
  min: 0,
  max: 100,
  palette: ['black', 'green']
};
Map.addLayer(dataset, treeCoverVisParam, 'tree cover');

// Entwaldung für 2017 und 2018 darstellen
var treeLossBefore = {
  bands: ['lossyear'],
  min: 0,
  max: 1,
  palette: ['blue']
};
Map.addLayer(notBolsonaro, treeLossBefore, 'tree loss before', true, 0.5);

// Entwaldung für 2018 und 2019 (Bolsonaro) darstellen
var treeLossBolsonaro = {
  bands: ['lossyear'],
  min: 0,
  max: 1,
  palette: ['red']
};
Map.addLayer(bolsonaro, treeLossBolsonaro, 'tree loss bolsonaro', true, 0.5);
